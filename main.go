package main

import (
	"fmt"
	"regexp"
	"time"
	"math"
	"strings"
	"encoding/json"
)

func one() {

	var number string
	num := "81-234-567-8901234,5" // you cange this value
	str := regexp.MustCompile(`[^a-zA-Z0-9 ]+`).ReplaceAllString(num, "")
	firstNum := str[0:1]

	if firstNum != "0" {

		number = "0" + str

		for i := 4; i < len(number); i += 5 {
			number = number[:i] + "-" + number[i:]
		}
		fmt.Println(number)
	}

	for i := 4; i < len(str); i += 5 {
		str = str[:i] + "-" + str[i:]
	}
	fmt.Println(number)

}

func two() {

	// you can change user's or message's value below
	chatRooms := `{
		"backend": [
			{
				"user": "Ari",
				"message": "Halo"
			},
			{
				"user": "Ariyanto",
				"message": "Halo juga"
			}
		],
		"frontend": [
			{
				"user": "suryanto",
				"message": "Lorem ipsum"
			},
			{
				"user": "Arindo",
				"message": "Helo"
			}
		]
	}`

	var input string = "halo" // or you can change this value too.

	var result []string
	var contents map[string][]map[string]string
	err := json.Unmarshal([]byte(chatRooms), &contents)
	if err != nil {
		fmt.Println("There's something wrong!")
		return
	}

	for rooms, chats := range contents {
		for _, chat := range chats {
			if strings.Contains(strings.ToLower(chat["user"]), strings.ToLower(input)) ||
				strings.Contains(strings.ToLower(chat["message"]), strings.ToLower(input)) {
				result = append(result, rooms)
				break
			}
		}
	}

	fmt.Println(result)

}

func three() {

	var toPage int = 1 // you can change this value
	var sumPage int = 231
	a := int(toPage/2)
	b := int(sumPage/toPage)
	fmt.Println(a, b)

	if a < b {
		fmt.Println("From first page flip ", a,"x")
	} else if a > b {
		fmt.Println("From first page flip ", b,"x")
	} else if a == b {
		fmt.Println("From first page flip ", a,"x")
	}

}

func four() {

	var year int = 2023
	var sameSchedule []time.Time
	meetSchedule := time.Date(year, time.April, 28, 0, 0, 0, 0, time.UTC)

	for i := 1; i <= 365; i++ {
		date := meetSchedule.AddDate(0, 0, i)
		if (date.Day()%12 == 0) && (date.Day()%14 == 0) && (date.Day()%6 == 0) {
			sameSchedule = append(sameSchedule, date)
			fmt.Print(sameSchedule)
		}
	}

	fmt.Println("Tanggal mereka akan berangkat bersama kembali dalam 1 tahun: ")
	for _, schedule := range sameSchedule {
		fmt.Println(schedule)
	}

}

func five() {

	var text string = "Aku wibu dan aku bangga" // you can change value, here.
	text = strings.ReplaceAll(text, " ", "")
	textLen := len(text)
	row := int(math.Sqrt(float64(textLen)))
	column := int(math.Ceil(float64(textLen) / float64(row)))

	matrix := make([][]string, row)
	for i := 0; i < row; i++ {
		matrix[i] = make([]string, column)
	}

	index := 0
	for i := 0; i < row; i++ {
		for j := 0; j < column; j++ {
			if index < textLen {
				matrix[i][j] = string(text[index])
				index++
			} else {
				break
			}
		}
	}

	encrRes := ""
	for j := 0; j < column; j++ {
		for i := 0; i < row; i++ {
			if matrix[i][j] != "" {
				encrRes += matrix[i][j]
			}
		}
		encrRes += " "
	}

	fmt.Println("Result:", encrRes)

}

func main()  {

	// plese choose what you wanna check base on test's number.
	five()
}